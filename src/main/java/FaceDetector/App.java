package FaceDetector;

import org.openimaj.image.FImage;
import org.openimaj.image.MBFImage;
import org.openimaj.image.colour.RGBColour;
import org.openimaj.image.colour.Transforms;
import org.openimaj.image.processing.face.detection.DetectedFace;
import org.openimaj.image.processing.face.detection.FaceDetector;
import org.openimaj.image.processing.face.detection.HaarCascadeDetector;
import org.openimaj.video.VideoDisplay;
import org.openimaj.video.VideoDisplayListener;
import org.openimaj.video.capture.VideoCapture;

import java.io.IOException;
import java.util.List;

public class App {
    public static void main(String[] args) throws IOException {

        VideoCapture vc = new VideoCapture(1000, 600);
        VideoDisplay<MBFImage> vd = VideoDisplay.createVideoDisplay(vc);

        vd.addVideoListener(new VideoDisplayListener<MBFImage>() {

            public void afterUpdate(VideoDisplay<MBFImage> videoDisplay) {


            }

            public void beforeUpdate(MBFImage fImages) {
                FaceDetector<DetectedFace,FImage> fd = new HaarCascadeDetector(150);

                List<DetectedFace> faces = fd.detectFaces(Transforms.calculateIntensity(fImages));
                //HaarCascadeDetector.BuiltInCascade e = HaarCascadeDetector.BuiltInCascade.eye;



                for (DetectedFace face:faces){
                    fImages.drawShape(face.getBounds(),10, RGBColour.BLUE);



                }

            }
        });

    }
}
