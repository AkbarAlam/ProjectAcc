package FaceKeyPoint;

import org.openimaj.image.FImage;
import org.openimaj.image.MBFImage;
import org.openimaj.image.colour.RGBColour;
import org.openimaj.image.colour.Transforms;
import org.openimaj.image.processing.face.detection.DetectedFace;
import org.openimaj.image.processing.face.detection.FaceDetector;
import org.openimaj.image.processing.face.detection.keypoints.FKEFaceDetector;
import org.openimaj.image.processing.face.detection.keypoints.FacialKeypoint;
import org.openimaj.image.processing.face.detection.keypoints.KEDetectedFace;
import org.openimaj.image.renderer.MBFImageRenderer;
import org.openimaj.image.typography.hershey.HersheyFont;
import org.openimaj.math.geometry.point.Point2dImpl;
import org.openimaj.math.geometry.shape.Shape;
import org.openimaj.video.VideoDisplay;
import org.openimaj.video.VideoDisplayListener;
import org.openimaj.video.capture.VideoCapture;
import org.openimaj.video.capture.VideoCaptureException;

import java.util.List;


public class AppKeyPoint {
    public static void main(String[] args) throws VideoCaptureException {
        VideoCapture vc = new VideoCapture(640, 500);
        VideoDisplay<MBFImage> vd = VideoDisplay.createVideoDisplay(vc);

        vd.addVideoListener(new VideoDisplayListener<MBFImage>() {


            public void afterUpdate(VideoDisplay<MBFImage> videoDisplay) {

            }

            public void beforeUpdate(MBFImage fImages) {

                FaceDetector<KEDetectedFace,FImage> keyFace = new FKEFaceDetector();
                List<KEDetectedFace> keyPointFace = keyFace.detectFaces(Transforms.calculateIntensity(fImages));

                for (DetectedFace face:keyPointFace) {
                    fImages.drawShape(face.getBounds(), 5, RGBColour.BLUE);



                    final Shape bounds=face.getBounds();
                    MBFImageRenderer renderer=fImages.createRenderer();

                    for (FacialKeypoint keypoint: ((KEDetectedFace)face).getKeypoints()) {

                        Point2dImpl pt = keypoint.position.clone();
                        pt.translate((float)bounds.minX(),(float)bounds.minY());
                        renderer.drawPoint(pt,RGBColour.RED,5);
                    }





                }
                try {
                    Thread.sleep(250);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
